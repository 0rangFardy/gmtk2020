#include <stdlib.h>
#include <string.h>

#include "raylib.h"
#include "raymath.h"

#define SCREEN_WIDTH (1600)
#define SCREEN_HEIGHT (900)

#define PLAYER_LENGTH (30)
#define PLAYER_SPEED (15)
#define PSPEED_DIAG_COMP (sqrtf(pow(PLAYER_SPEED, 2) / 2)) //for diagonal motion
#define SPEED_NO_CONTROL (30) //different speed when out of control
#define SPEED_NC_DIAG_COMP (sqrtf(pow(SPEED_NO_CONTROL, 2) / 2))

#define DANGER_BORDER (90)
#define BORDER_COLOR ((Color){139, 0, 0, 255})

#define ENEMY_RADIUS (21)
#define ENEMY_SPEED (10)

#define ARRAY_BUFF (8)

#define ASSET_PATH "../assets/"

struct HazardRect{}; //if you're feeling ambitious :)

enum Input{
    UP,
    UP_RIGHT,
    RIGHT,
    DOWN_RIGHT,
    DOWN,
    DOWN_LEFT,
    LEFT,
    UP_LEFT,
};

//
void spawn_enemy(){

}

int main(void)
{
    // Initialization
    Rectangle player = {(float) SCREEN_WIDTH / 2, (float) SCREEN_HEIGHT / 2,
                        PLAYER_LENGTH, PLAYER_LENGTH};
    Color player_colour = BLUE;
    float player_speed = PLAYER_SPEED;
    enum Input playerchar_input;
    
    Vector2* enemies = (Vector2*)calloc(ARRAY_BUFF, sizeof(Vector2));
    int buffer_count = 1;
    int enemy_count = 0;
    Vector2 zero_vector = {0};
    
    enemies[0] = (Vector2){50, 50};
    
    Rectangle danger_border_top = {SCREEN_WIDTH - DANGER_BORDER};
    
    bool game_start = false;
    bool in_control = false;

    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Move It or Lose It");

    SetTargetFPS(60); // Set our game to run at 60 frames-per-second

    // Main game loop
    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        //hold off on input shenanigans until they press space
        if (!game_start) {
            player.x = (float) SCREEN_WIDTH / 2;
            player.y = (float) SCREEN_HEIGHT / 2;
            if (IsKeyPressed(KEY_SPACE)) { game_start = true; }
        }
        if (game_start){
            in_control = false;
            player_colour = BLUE;
            Vector2 player_centre = {player.x + player.width/2,
                                     player.y + player.height/2};
            
            //handle human input
            {
                bool kd_up = IsKeyDown(KEY_UP);
                bool kd_right = IsKeyDown(KEY_RIGHT);
                bool kd_down = IsKeyDown(KEY_DOWN);
                bool kd_left = IsKeyDown(KEY_LEFT);
        
                if (kd_left && kd_up && !kd_right && !kd_down)
                {
                    playerchar_input = UP_LEFT;
                    in_control = true;
                }
                if (kd_up && kd_right && !kd_left && !kd_down)
                {
                    playerchar_input = UP_RIGHT;
                    in_control = true;
                }
                if (kd_down && kd_right && !kd_up && !kd_left)
                {
                    playerchar_input = DOWN_RIGHT;
                    in_control = true;
                }
                if (kd_left && kd_down && !kd_right && !kd_up)
                {
                    playerchar_input = DOWN_LEFT;
                    in_control = true;
                }
                if (kd_up && !kd_right && !kd_down && !kd_left)
                {
                    playerchar_input = UP;
                    in_control = true;
                }
                if (kd_right && !kd_down && !kd_left && !kd_up)
                {
                    playerchar_input = RIGHT;
                    in_control = true;
                }
                if (kd_down && !kd_left && !kd_up && !kd_right)
                {
                    playerchar_input = DOWN;
                    in_control = true;
                }
                if (kd_left && !kd_up && !kd_right && !kd_down)
                {
                    playerchar_input = LEFT;
                    in_control = true;
                }
            }
            
            if (!in_control){
                playerchar_input = GetRandomValue(0, 7);
                player_colour = PURPLE;
            }
    
            switch (playerchar_input) //actual movement
            {
                case UP:
                    player.y -= in_control ? PLAYER_SPEED : SPEED_NO_CONTROL;
                    break;
                case RIGHT:
                    player.x += in_control ? PLAYER_SPEED : SPEED_NO_CONTROL;
                    break;
                case LEFT:
                    player.x -= in_control ? PLAYER_SPEED : SPEED_NO_CONTROL;
                    break;
                case DOWN:
                    player.y += in_control ? PLAYER_SPEED : SPEED_NO_CONTROL;
                    break;
                case UP_RIGHT:
                    player.x += in_control ? PSPEED_DIAG_COMP : SPEED_NC_DIAG_COMP;
                    player.y -= in_control ? PSPEED_DIAG_COMP : SPEED_NC_DIAG_COMP;
                    break;
                case DOWN_RIGHT:
                    player.x += in_control ? PSPEED_DIAG_COMP : SPEED_NC_DIAG_COMP;
                    player.y += in_control ? PSPEED_DIAG_COMP : SPEED_NC_DIAG_COMP;
                    break;
                case DOWN_LEFT:
                    player.x -= in_control ? PSPEED_DIAG_COMP : SPEED_NC_DIAG_COMP;
                    player.y += in_control ? PSPEED_DIAG_COMP : SPEED_NC_DIAG_COMP;
                    break;
                case UP_LEFT:
                    player.x -= in_control ? PSPEED_DIAG_COMP : SPEED_NC_DIAG_COMP;
                    player.y -= in_control ? PSPEED_DIAG_COMP : SPEED_NC_DIAG_COMP;
                    break;
            }
            
            //make enemies chase player, manage collision too
            for(int i = 0; i < ARRAY_BUFF * buffer_count; i++){
                if (memcmp(enemies+i, &zero_vector, sizeof(Vector2)))
                {
                    Vector2 enemy_to_player = {player_centre.x - enemies[i].x,
                                               player_centre.y - enemies[i].y};
                    float distance_to_player = Vector2Length(enemy_to_player);
    
                    if (distance_to_player < ENEMY_RADIUS)
                    {
                        if (in_control) { game_start = false; }
                        else {
                            enemies[i] = Vector2Zero();
                        }
                    }
                    
                    float ratio = distance_to_player / ENEMY_SPEED;
        
                    Vector2 enemy_movement = {enemy_to_player.x / ratio,
                                              enemy_to_player.y / ratio};
                    enemies[i] = Vector2Add(enemies[i], enemy_movement);
                }
            }
            
            
            
            //necessary border control
            if (player.x > SCREEN_WIDTH - DANGER_BORDER){ game_start = false; }
            else if (player.x < DANGER_BORDER - PLAYER_LENGTH ){game_start = false;}
            if (player.y > SCREEN_HEIGHT - DANGER_BORDER) { game_start = false;}
            else if (player.y < DANGER_BORDER - PLAYER_LENGTH) {game_start = false;}
            
            //UNNECESSARY BORDER CONTROL!!!!
            if (player.x < 0){player.x = 0;}
            else if (player.x > SCREEN_WIDTH - PLAYER_LENGTH) {player.x = SCREEN_WIDTH - PLAYER_LENGTH;}
            if (player.y < 0){player.y = 0;}
            else if (player.y > SCREEN_HEIGHT - PLAYER_LENGTH){player.y = SCREEN_HEIGHT - PLAYER_LENGTH;}
            
        }
        
        
        BeginDrawing();

        ClearBackground(GRAY);
        
        //Draw the danger borders
        DrawRectangle(0, 0, DANGER_BORDER, SCREEN_HEIGHT, BORDER_COLOR); // LEFT
        DrawRectangle(0,0, SCREEN_WIDTH, DANGER_BORDER, BORDER_COLOR); // TOP
        DrawRectangle(0, SCREEN_HEIGHT - DANGER_BORDER, SCREEN_WIDTH, DANGER_BORDER, BORDER_COLOR); //BOTTOM
        DrawRectangle(SCREEN_WIDTH - DANGER_BORDER, 0, DANGER_BORDER, SCREEN_HEIGHT, BORDER_COLOR); //RIGHT
        
        //DrawCircleV(enemy_pos, ENEMY_RADIUS, RED);
    
        for (int i = 0; i < ARRAY_BUFF * buffer_count; i++)
        {
            if (memcmp(enemies + i, &zero_vector, sizeof(Vector2)))
            {
                DrawCircleV(enemies[i], ENEMY_RADIUS, RED);
            }
        }
        
        if (!game_start){DrawText("Press SPACE to Begin", SCREEN_WIDTH/2 - 150, SCREEN_HEIGHT * 0.75, 30, RED);}
    
        DrawRectangleRec(player, game_start ? player_colour : RED);
        
        
        
        EndDrawing();
    }

    // De-Initialization

    CloseWindow(); // Close window and OpenGL context

    return 0;
}